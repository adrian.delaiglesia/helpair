 #helpAir 
 
 helpAir es un proyecto de respirador por control de volumen de bajo coste.
 Permite configurar:
 - el volumen tidal (% de recorrido)
 - la frecuencia de respiración
 - la proporción inspiración-espiración
 - la proporcion de pausa
desde un único menú de selección.

El proyecto se ha desarrollado de manera espontánea por un grupo de empleados de Ferrovial y Sngular
con el apoyo de http://respiradores4all.com

Actualmente existen dos versiones del proyecto en función del mecanismo de impulsión de aire
- mecanismo de leva y bolsa de respiración
- mecanismo de husillo y Ambú

##Documentación
- documento presentación del proyecto
- documento descriptivo del proyecto

##Electrónica
- BOM Bill of Materials
- esquema conexiones

##Código
- código implementado en el controlador arduino

##Diseños 3D
- Archivos STL versión leva
- Archivos STL versión husillo


#Equipo del proyecto

- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio Losas Dávila
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

 
 